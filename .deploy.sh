#!/bin/bash
[[ -f /.deploy.sh.env ]] && export $(cat /.deploy.sh.env | xargs);

# ---------------------------------------------------------------------------- #
#                               variabel-variabel                              #
# ---------------------------------------------------------------------------- #
REPOSITORY="https://$REPOSITORY_TOKEN@gitlab.com/web-api-pemko-gusit/stapi.git";
APP_DIR="/var/www/app";
RELEASES_CONTAINER_DIR="$APP_DIR/releases";
RELEASE_NAME=$(date +'%Y%m%d%H%M%S');
NEW_RELEASE_DIR="$RELEASES_CONTAINER_DIR/$RELEASE_NAME";

# ---------------------------------------------------------------------------- #
#                                menyiapkan .env                               #
# ---------------------------------------------------------------------------- #
if [[ ! -f "$APP_DIR/.env" ]];
then
    echo "File $APP_DIR/.env tidak ada!";
    exit 1;
fi
chown www-data: "$APP_DIR/.env";

# ---------------------------------------------------------------------------- #
#                               clone repository                               #
# ---------------------------------------------------------------------------- #
[[ ! -d $RELEASES_CONTAINER_DIR ]] && mkdir -p $RELEASES_CONTAINER_DIR;
apt-get install -y git unzip;
git clone --depth 1 $REPOSITORY $NEW_RELEASE_DIR;
ln -nfs "$APP_DIR/.env" "$NEW_RELEASE_DIR/.env";
cd "$NEW_RELEASE_DIR";

# ---------------------------------------------------------------------------- #
#                              project dependency                              #
# ---------------------------------------------------------------------------- #
composer install --optimize-autoloader --no-dev;
php artisan optimize:clear;

# ---------------------------------------------------------------------------- #
#                                 setup storage                                #
# ---------------------------------------------------------------------------- #
[[ ! -d "$APP_DIR/storage" ]] && cp -r "$NEW_RELEASE_DIR/storage" "$APP_DIR/";
chown -R www-data: "$APP_DIR/storage";
chmod -R 777 "$APP_DIR/storage";
rm -rf "$NEW_RELEASE_DIR/storage";
ln -nfs "$APP_DIR/storage" "$NEW_RELEASE_DIR/storage";

# ---------------------------------------------------------------------------- #
#                              migration dan seed                              #
# ---------------------------------------------------------------------------- #
php artisan migrate --step --force
#php artisan db:seed --force

# ---------------------------------------------------------------------------- #
#                                   finishing                                  #
# ---------------------------------------------------------------------------- #
chown -R www-data: "$RELEASES_CONTAINER_DIR";
chmod -R 755 "$RELEASES_CONTAINER_DIR";
chmod -R 777 "$NEW_RELEASE_DIR/storage";
su -s /bin/bash -c 'php artisan config:cache' www-data;
su -s /bin/bash -c 'php artisan event:cache' www-data;
su -s /bin/bash -c 'php artisan route:cache' www-data;
su -s /bin/bash -c 'php artisan view:cache' www-data;
su -s /bin/bash -c 'php artisan optimize' www-data;

# ---------------------------------------------------------------------------- #
#                                 final linking                                #
# ---------------------------------------------------------------------------- #
ln -nfs "$NEW_RELEASE_DIR" "/var/www/html";
