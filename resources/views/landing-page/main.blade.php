@extends('home')

@section('container')
    <!-- ======= Header ======= -->
    <header id="header" class="header fixed-top">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

            <a href="/" class="logo d-flex align-items-center">
                <img src="{{ url('assets') }}/img/logo-web-api.png" alt="">
            </a>
            @include('landing-page.components.navbar')
        </div>
    </header><!-- End Header -->
    
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="hero d-flex align-items-center">

        @include('landing-page.components.hero')

    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= Values Section ======= -->
        <section id="overviews" class="values">

            <div class="container" data-aos="fade-up">

                <header class="section-header">
                    <h2>Gambaran Umum</h2>
                    <p>Aplikasi <i>web</i> API Pemerintah Kota Gunungsitoli</p>
                </header>

                <div class="row">

                    <div class="col-lg-4" data-aos="fade-up" data-aos-delay="200">
                        <div class="box">
                            <img src="{{ url('assets') }}/img/values-1.png" class="img-fluid" alt="">
                            <h3>Tempat berkolaborasi antar Organisasi Pemerintah Daerah</h3>
                        </div>
                    </div>

                    <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="400">
                        <div class="box">
                            <img src="{{ url('assets') }}/img/values-2.png" class="img-fluid" alt="">
                            <h3>Kemudahan dalam komunikasi data</h3>
                        </div>
                    </div>

                    <div class="col-lg-4 mt-4 mt-lg-0" data-aos="fade-up" data-aos-delay="600">
                        <div class="box">
                            <img src="{{ url('assets') }}/img/values-3.png" class="img-fluid" alt="">
                            <h3>Bentuk implementasi Satu Data Indonesia</h3>
                        </div>
                    </div>

                </div>

            </div>

        </section><!-- End Values Section -->

        <!-- ======= Features Section ======= -->
        <section id="features" class="features">

            <div class="container" data-aos="fade-up">

                <header class="section-header">
                    <h2>Fitur</h2>
                    <p>Beberapa fungsi untuk berkolaborasi dan berkomunkasi terkait data dalam aplikasi <i>web</i> API
                        ini</p>
                </header>

                <div class="row">

                    <div class="col-lg-6">
                        <img src="{{ url('assets') }}/img/features.png" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-6 mt-5 mt-lg-0 d-flex">
                        <div class="row align-self-center gy-4">

                            <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                                <div class="feature-box d-flex align-items-center">
                                    <i class="bi bi-check"></i>
                                    <h3>Akses data lebih cepat</h3>
                                </div>
                            </div>

                            <div class="col-md-6" data-aos="zoom-out" data-aos-delay="300">
                                <div class="feature-box d-flex align-items-center">
                                    <i class="bi bi-check"></i>
                                    <h3>Variasi data yang banyak</h3>
                                </div>
                            </div>

                            <div class="col-md-6" data-aos="zoom-out" data-aos-delay="400">
                                <div class="feature-box d-flex align-items-center">
                                    <i class="bi bi-check"></i>
                                    <h3>Pembaharuan data yang terakselerasi</h3>
                                </div>
                            </div>

                            <div class="col-md-6" data-aos="zoom-out" data-aos-delay="500">
                                <div class="feature-box d-flex align-items-center">
                                    <i class="bi bi-check"></i>
                                    <h3>Sumber data yang terintegrasi</h3>
                                </div>
                            </div>

                        </div>
                    </div>

                </div> <!-- / row -->

            </div>

        </section><!-- End Features Section -->

        <!-- ======= Clients Section ======= -->
        <section id="lembaga" class="clients">

            <div class="container" data-aos="fade-up">

                <header class="section-header">
                    <h2>Lembaga Partner</h2>
                    <p>Beberapa lembaga yang berkontribusi</p>
                </header>

                <div class="clients-slider swiper">
                    <div class="swiper-wrapper align-items-center">
                        <div class="swiper-slide"><img src="{{ url('assets') }}/img/clients/kemenkeu.png" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{ url('assets') }}/img/clients/bkn.png" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{ url('assets') }}/img/clients/bps-ri.png" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{ url('assets') }}/img/clients/panrb.png" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{ url('assets') }}/img/clients/pupr.jpg" class="img-fluid"
                                alt=""></div>
                        <div class="swiper-slide"><img src="{{ url('assets') }}/img/clients/perpusnas.svg"
                                class="img-fluid" alt=""></div>
                        <div class="swiper-slide"><img src="{{ url('assets') }}/img/clients/kominfo.svg"
                                class="img-fluid" alt=""></div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>

        </section><!-- End Clients Section -->

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">
        @include('landing-page.components.footer')
        <div class="container">
            <div class="copyright">
                &copy; Made with ❤️ <strong><span>Pemerintah Kota Gunungsitoli</span></strong> {{ date('Y') }}. All
                Rights Reserved
            </div>
            <div class="credits">
                Template by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </footer><!-- End Footer -->
@endsection
